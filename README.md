# Presentation

Generate static site using [mkdocs](https://www.mkdocs.org/).

# Quick start

Clone the repository, create a branch for your site and run `make all`

    git checkout -b your.site 
    make all
    git add mkdocs.yaml docs

Edit files for your site, test using `make run`, commit changes ...

When changes are ok, build static site `make build` then 
copy static content from `site/` to remote server.

