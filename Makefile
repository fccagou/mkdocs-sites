prepare:
	# Prépare local env
	[ -d venv ] && true || python3 -m venv venv

install:
	#install commands
	[ -f venv/bin/activate ] && . venv/bin/activate
	pip install --upgrade pip && \
		pip install -r requirements.txt

build:
	[ -f venv/bin/activate ] && . venv/bin/activate
	[ -f mkdocs.yml ] || mkdocs new .
	mkdocs build

run:
	#serve doc
	mkdocs serve

deploy:
	# deploy scp or rsync site/* to remote server
	source ./.env && deploy

all: prepare install build deploy
